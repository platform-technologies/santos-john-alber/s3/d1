/* script */

function addNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 + num2;
	return false;
}

function subNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 - num2;
	return false;
}

function mulNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 * num2;
	return false;
}

function divNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 / num2;
	return false;
}

function modNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 % num2;
	return false;
}

function Clear(){
	location.reload(true);
}

document.getElementById('add').addEventListener('click', addNumbers);
document.getElementById('sub').addEventListener('click', subNumbers);
document.getElementById('mul').addEventListener('click', mulNumbers);
document.getElementById('div').addEventListener('click', divNumbers);
document.getElementById('mod').addEventListener('click', modNumbers);
document.getElementById('cle').addEventListener('click', Clear);
